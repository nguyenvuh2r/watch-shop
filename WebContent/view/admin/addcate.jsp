<%@page contentType="text/html" pageEncoding="UTF-8"%>
  <!-- Start header section -->
  <jsp:include page = "./header/header.jsp" flush = "true" />
    <div class="content-wrapper">
      <div class="container-fluid">

        <div class="row mt-3">
          <div class="col-lg-8">
            <div class="card">
              <div class="card-body">
                <div class="card-title">Thêm chuyên mục</div>
                <hr>
                <form id="addForm" action="${pageContext.request.contextPath}/admin/cate/add" method="post">
                
                  <div class="form-group">
                    <label for="cate-name">Tên chuyên mục</label>
                    <input type="text" class="form-control" id="cate-name" placeholder="Tên chuyên mục" name="cate-name">
                  </div>
	               <div class="form-group">
	                  <label for="parent-id">Chuyên mục cha</label>
	                  <div>
	                    <select class="form-control valid" id="parent-id" name="parent-id" aria-invalid="false">
	                        <option value="0">NULL</option>
	                       <option value="1">Sản phẩm mới</option>
	                    </select>
	                  </div>
	                </div>
                 <div class="form-footer">
                    <button type="submit" class="btn btn-danger"><i class="fa fa-times"></i> Hủy</button>
                    <button type="submit" class="btn btn-success"><i class="fa fa-check-square-o"></i> Thêm</button>
                </div>
                </form>
              </div>
            </div>
          </div>
        </div>
        <div class="overlay toggle-menu"></div>
      </div>
    </div>
  	<script>
  		$().ready(function(){
  			$("#addForm").validate({
  				rules:{
  					"cate-name":{
  						required:true
  					},
  				},
  				messages:{
  					"cate-name":{
  						required: "Hãy nhập tên chuyên mục"
  					},
  				}
  			})
  		})
  	</script>
    <jsp:include page = "./footer/footer.jsp" flush = "true" />