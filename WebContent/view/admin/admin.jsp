<%@page import="java.sql.ResultSet"%>
<%@page import="watch.webmvc.jdbc.connectDB"%>
<%@page language="java" contentType="text/html; charset=utf-8" pageEncoding="UTF-8"%> 
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %> 
<%
  response.setHeader("Cache-control", "no-cache, no-store, must-revalidate");
  response.setHeader("Pragma" , "no-cache");
  response.setHeader("Expires" , "0");
  
  
  if (session.getAttribute("admin-username") == null){
	  response.sendRedirect(request.getContextPath() + "/admin/login"); 
  }
  %>
  <!-- Start header section --> 
  <jsp:include page = "./header/header.jsp" flush = "true" /> 
    <div class="content-wrapper"> 
      <div class="container-fluid"> 
        <!--End Row--> 
 
 
        <div class="row"> 
          <div class="col-lg-12"> 
            <button class="add-catalog"><a href="${pageContext.request.contextPath}/admin/admin/add">Thêm Admin</a> 
          </div> 
           <div><b> <span style="color:#dc203a"> ${errorMessage}</span></b></div>
          <div class="col-lg-12"> 
            <div class="card"> 
              <div class="card-body"> 
                <h5 class="card-title">Danh sách Admin</h5> 
                <div class="table-responsive">              
                  <table class="table table-striped" id="table_id"> 
                    <thead> 
                      <tr> 
                        <th scope="col">#</th> 
                        <th scope="col">Tên đăng nhập</th> 
                        <th scope="col">Tên Admin</th>
                        <th scope="col">Hành động</th>                        
                     </tr> 
                    </thead> 
                    <tbody> 
                  <c:forEach items="${adminlist}" var="admin"> 
                      <tr> 
                        <td scope="row">${admin.id}</td> 
                        <td>${admin.username}</td> 
        				<td>${admin.name}</td> 
        				 <td> 
                           <a class="btn btn-danger" href="${pageContext.request.contextPath}/admin/admin/delete?admin-id=${admin.id}">Xóa</a>
              
                          <a class= "btn btn-success" href="${pageContext.request.contextPath}/admin/admin/edit?id=${admin.id}">Sửa</a>
                        </td> 
                     </tr> 
                    </c:forEach> 
                    </tbody> 
                  </table> 
                </div> 
              </div> 
            </div> 
          </div> 
        </div> 
      </div> 
    </div> 
   <style>
table.dataTable tbody tr {
	background-color: unset;
}
</style>
<script>
	$(document).ready(function() {
		$('#table_id').DataTable({});
	});
	
</script>
    <jsp:include page = "./footer/footer.jsp" flush = "true" />