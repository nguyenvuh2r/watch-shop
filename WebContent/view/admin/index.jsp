<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
  response.setHeader("Cache-control", "no-cache, no-store, must-revalidate");
  response.setHeader("Pragma" , "no-cache");
  response.setHeader("Expires" , "0");
  
  
  if (session.getAttribute("admin-username") == null){
	  response.sendRedirect(request.getContextPath() + "/admin/login"); 
  }
  %>
  <!-- Start header section -->
  <jsp:include page = "./header/header.jsp" flush = "true" />
    <div class="content-wrapper">
      <div class="container-fluid">
        <div class="card mt-3">
          <div class="card-content">
          	<div class="row row-group m-0">
              <div class="col-12 col-lg-6 col-xl-4 border-light">
                <div class="card-body">
                  <h5 class="text-white mb-0">${bill_in_week} <span class="float-right"><i class="fa fa-shopping-cart"></i></span>
                  </h5>
                  <div class="progress my-3" style="height:3px;">
                    <div class="progress-bar" style="width:${grow}%"></div>
                  </div>
                  <p class="mb-0 text-white small-font">Đơn hàng (theo tuần)<span class="float-right">+${grow}% <i
                        class="zmdi zmdi-long-arrow-up"></i></span></p>
                </div>
              </div>
              <div class="col-12 col-lg-6 col-xl-4 border-light">
                <div class="card-body">
                  <h5 class="text-white mb-0">25 <span class="float-right"><i class="fa fa-eye"></i></span></h5>
                  <div class="progress my-3" style="height:3px;">
                    <div class="progress-bar" style="width:55%"></div>
                  </div>
                  <p class="mb-0 text-white small-font">Ghé trang (theo tuần)<span class="float-right">+5.2% <i
                        class="zmdi zmdi-long-arrow-up"></i></span></p>
                </div>
              </div>
              <div class="col-12 col-lg-6 col-xl-4 border-light">
                <div class="card-body">
                  <h5 class="text-white mb-0">${item_in_week} <span class="float-right"><i class="fa fa-envira"></i></span></h5>
                  <div class="progress my-3" style="height:3px;">
                    <div class="progress-bar" style="width:${item_grow}%"></div>
                  </div>
                  <p class="mb-0 text-white small-font">Bán ra (theo tuần)<span class="float-right">+${item_grow}% <i
                        class="zmdi zmdi-long-arrow-up"></i></span></p>
                </div>
              </div>
            </div>
          </div>
        </div>
        
        <div class="row">
          <div class="col-12 col-lg-12 col-xl-12">
            <div class="card">
              <div class="card-header">Danh mục bán chạy
              </div>
              <div class="card-body">
                <div class="chart-container-2">
                  <canvas id="chart2"></canvas>
                </div>
              </div>
              <div class="table-responsive">
                <table class="table">
                  <tbody>
                  	<c:set var="count" value="0" scope="page" />
	                <c:forEach var="product" items="${lstProduct}">
				      <tr>
				      	<td><i class="fa fa-circle text-white mr-2"></i>${product}</td>
                      	<td align="right">${lstProductQty[count]} $</td>
				      </tr>
				      <c:set var="count" value="${count + 1}" scope="page"/>
				   	</c:forEach>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <a href="javaScript:void();" class="back-to-top"><i class="fa fa-angle-double-up"></i> </a>
    <div class="right-sidebar">
      <div class="switcher-icon">
        <i class="zmdi zmdi-settings zmdi-hc-spin"></i>
      </div>
      <div class="right-sidebar-content">
        <p class="mb-0">Màu nền admin</p>
        <hr>
        <ul class="switcher">
          <li id="theme1"></li>
          <li id="theme2"></li>
          <li id="theme3"></li>
          <li id="theme4"></li>
          <li id="theme5"></li>
          <li id="theme6"></li>
        </ul>
        <p class="mb-0">Màu nền gradient</p>
        <hr>
        <ul class="switcher">
          <li id="theme7"></li>
          <li id="theme8"></li>
          <li id="theme9"></li>
          <li id="theme10"></li>
          <li id="theme11"></li>
          <li id="theme12"></li>
          <li id="theme13"></li>
          <li id="theme14"></li>
          <li id="theme15"></li>
        </ul>
      </div>
    </div>
  </div>
  <script>
  	$().ready(function() {
  		$(function() {
  		    "use strict";
  		    // chart 2
  				var ctx = document.getElementById("chart2").getContext('2d');
  					var myChart = new Chart(ctx, {
  						type: 'doughnut',
  						data: {
  							labels: [${char_labels}],
  							datasets: [{
  								backgroundColor: [${char_background}],
  								data: [${char_data}],
  								borderWidth: [0, 0, 0, 0]
  							}]
  						},
  					options: {
  						maintainAspectRatio: false,
  					   legend: {
  						 position :"bottom",	
  						 display: false,
  						    labels: {
  							  fontColor: '#ddd',  
  							  boxWidth:15
  						   }
  						}
  						,
  						tooltips: {
  						  displayColors:false
  						}
  					   }
  					});
  		   });	 
  	})
  	
  	function getRandomColor() {
	  var letters = '0123456789ABCDEF';
	  var color = '#';
	  for (var i = 0; i < 6; i++) {
	    color += letters[Math.floor(Math.random() * 16)];
	  }
	  return color;
	}
  </script>
    <jsp:include page = "./footer/footer.jsp" flush = "true" />