<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
  <!-- Start header section -->
  <jsp:include page = "./header/header.jsp" flush = "true" />
<style>
#product-discount-error{
width: 100%
}
</style>
    <div class="content-wrapper">
      <div class="container-fluid">
        <div class="row mt-3">
          <div class="col-lg-12">
            <div class="card">
              <div class="card-body">
                <div class="card-title">Thêm sản phẩm</div>
                <hr>
                <form id="addForm" method="post" action="${pageContext.request.contextPath}/admin/product/add" enctype='multipart/form-data'>
                  <div class="form-group">
                    <label for="product-name">Tên sản phẩm</label>
                    <input type="text" class="form-control" id="product-name" placeholder="Tên sản phẩm" name="product-name">
                  </div>
                  <div class="form-group">
	                  <label for="input-2">Chuyên mục</label>
	                  <div>
	                    <select class="form-control valid" id="input-6" name="product-cate" aria-invalid="false">
	                    <c:forEach items="${catelist}" var="cate">
	                        <option value="${cate.id }">${cate.name }</option>
	                    </c:forEach>
	                    </select>
	                  </div>
	                </div>
	                 <div class="form-group">
                    <label for="product-day">Ngày</label> 
                    <input type="date" class="form-control" id="product-day" placeholder="Ngày đăng" name="product-day">
                  </div>
	                <div class="form-group">
                    <label for="product-price">Giá</label>
                    <input type="text"  class="form-control" id="product-price" pattern="^\$\d{1,3}(,\d{3})*(\.\d+)?$" value="" data-type="currency" placeholder="Giá" name="product-price">
                  </div>
                    <div class="form-group">
	                  <label for="product-status">Trạng thái</label>
	                  <div>
	                    <select class="form-control valid" id="product-status" name="product-status" required aria-invalid="false">
	                        <option value="1">Còn hàng</option>
	                        <option value="0">Hết hàng</option>
	                    </select>
	                  </div>
	                </div>
	                 <div class="form-group">
		                <label for="product-discount">Giảm giá</label>
		                <div class="input-group">
		                <input type="text" class="form-control " placeholder="Giảm ... %" name="product-discount">
		                </div>
	              </div>
                 <div class="form-group">
                  <label for="product-desc" class="col-form-label">Mô tả</label>
                  <div>
                    <textarea class="form-control" rows="4" id="product-desc" name="product-desc"></textarea>
                  </div>
                </div>
                
                <div class="form-group">
                  <label for="product-content" class="col-form-label">Nội dung</label>
                  <div>
                    <textarea class="form-control" rows="4" id="product-content" name="product-content"></textarea>
                  </div>
                </div>
                
                 <div class="form-group">
                    <label for="input-1">Ảnh sản phẩm</label>
                    <input type="file" class="form-control" id="input-1" placeholder="Tên hình" name="file" multiple>
                  </div>
               <div class="form-footer">
                    <button class="btn btn-danger"><i class="fa fa-times"></i><a href="${pageContext.request.contextPath}/admin/product/list">Hủy</a>
                    <button type="submit" class="btn btn-success"><i class="fa fa-check-square-o"></i> Thêm</button>
                </div>
                </form>
              </div>
            </div>
          </div>
        </div>
        <div class="overlay toggle-menu"></div>
      </div>
    </div>
<script>



$("input[data-type='currency']").on({
    keyup: function() {
      formatCurrency($(this));
    },
    blur: function() { 
      formatCurrency($(this), "blur");
    }
});


function formatNumber(n) {
  // format number 1000000 to 1,234,567
  return n.replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ",")
}


function formatCurrency(input, blur) {
  // appends $ to value, validates decimal side
  // and puts cursor back in right position.
  
  // get input value
  var input_val = input.val();
  
  // don't validate empty input
  if (input_val === "") { return; }
  
  // original length
  var original_len = input_val.length;

  // initial caret position 
  var caret_pos = input.prop("selectionStart");
    
  // check for decimal
  if (input_val.indexOf(".") >= 0) {

    // get position of first decimal
    // this prevents multiple decimals from
    // being entered
    var decimal_pos = input_val.indexOf(".");

    // split number by decimal point
    var left_side = input_val.substring(0, decimal_pos);
    var right_side = input_val.substring(decimal_pos);

    // add commas to left side of number
    left_side = formatNumber(left_side);

    // validate right side
    right_side = formatNumber(right_side);
    
    // On blur make sure 2 numbers after decimal
    if (blur === "blur") {
      right_side += "";
    }
    
    // Limit decimal to only 2 digits
    right_side = right_side.substring(0, 2);

    // join number by .
    input_val =  left_side + "." + right_side;

  } else {
    // no decimal entered
    // add commas to number
    // remove all non-digits
    input_val = formatNumber(input_val);
    input_val =  input_val;
    
    // final formatting
    if (blur === "blur") {
      input_val += "";
    }
  }
  
  // send updated string to input
  input.val(input_val);

  // put caret back in the right position
  var updated_len = input_val.length;
  caret_pos = updated_len - original_len + caret_pos;
  input[0].setSelectionRange(caret_pos, caret_pos);
}

		$().ready(function(){
			$("#addForm").validate({
				rules: {
					"product-name":{
						required: true,
						minlength: 2,
					},
					"product-day":{
						required: true,
					},
					"product-price":{
						required: true,
						maxlength: 8
					},
					"product-discount":{
						number: true,
						max: 99,
					},
					"product-desc":{
						required: true,
					},
					"product-content":{
						required: true,
					},
				},
				messages: {
					"product-name":{
						required: "Hãy nhập tên sản phẩm",
						minlength: "Tên sản phẩm ít nhất 2 kí tự",
					},
					"product-day":{
						required: "Hãy nhập ngày tạo",
					},
					"product-price":{
						required: "Hãy nhập giá sản phẩm",
						 maxlength: "Hãy nhập ít hơn 6 số"
					},
					"product-discount":{
						number: "Hãy nhập dưới dạng số (number)",
						max: "Giam giá tối đa dưới 99%",
					},
					"product-desc":{
						required: "Hãy nhập mô tả sản phẩm",
					},
					"product-content":{
						required: "Hãy nhập nội dung sản phẩm",
					}
				}
			});
		});
		

		




</script>
    <jsp:include page = "./footer/footer.jsp" flush = "true" />