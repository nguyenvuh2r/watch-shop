<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
  <!-- Start header section -->
  <jsp:include page = "./header/header.jsp" flush = "true" />

    <div class="content-wrapper">
      <div class="container-fluid">

        <div class="row mt-3">
          <div class="col-lg-12">
            <div class="card">
              <div class="card-body">
                <div class="card-title">Thêm User</div>
                <hr>
                <form id="addForm" action="${pageContext.request.contextPath}/admin/user/add" method="post">
                
                  <div class="form-group">
                    <label for="user-name">Họ tên</label>
                    <input type="text" class="form-control" id="user-name" placeholder="Nhập họ tên" name="user-name">
                  </div>
                  <div class="form-group">
                    <label for="user-email">Email</label>
                    <input type="text" class="form-control" id="user-email" placeholder="Nhập địa chỉ Email" name="user-email">
                  </div>
                  <div class="form-group">
                    <label for="user-phone">Số Điện Thoại</label>
                    <input type="text" class="form-control" id="user-phone" placeholder="Nhập số điện thoại" name="user-phone">
                  </div>
                  <div class="form-group">
                    <label for="user-userName">UserName</label>
                    <input type="text" class="form-control" id="user-userName" placeholder="Nhập User Name" name="user-userName">
                  </div>
                  <div class="form-group">
                    <label for="user-password">Mật khẩu</label>
                    <input type="password" class="form-control" id="user-password" placeholder="Nhập mật khẩu" name="user-password">
					<input type="checkbox" onclick="myFunction1()">Hiển thị mật khẩu
					<script>function myFunction1() {
                    	  var x = document.getElementById("user-password");
                    	  if (x.type === "password") {
                    	    x.type = "text";
                    	  } else {
                    	    x.type = "password";
                    	  }
                    	}
					</script>                   
                  </div>
                  <div class="form-group">
                    <label for="user-created">Date</label>
                    <input type="date" class="form-control" id="user-created" placeholder="Ngày tạo" name="user-created">
                  </div>
                  <div class="form-group">
                    <button type="submit" class="btn btn-light px-5"><i class="icon-lock"></i> Đăng ký</button>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
        <div class="overlay toggle-menu"></div>
      </div>
    </div>
    <script>
    $().ready(function(){
    	$("#addForm"}).validate({
    		rules:{
    			"user-name":{
    				required: true
    			},
        		"user-email":{
    				required: true,
    				email: true
    			},
        		"user-phone":{
    				required: true,
    				rangelength: [10, 10]
    			},
        		"user-userName":{
    				required: true,
    				minlength: 2
    			},
        		"user-password":{
    				required: true,
    				minlength: 2
    			},
    			"user-created":{
    				required: true,
    			}
    		},
    		messages: {
    			"user-name":{
    				required: "Hãy nhập họ và tên"
    			},
        		"user-email":{
    				required: "Hãy nhập e-mail",
    				email: "E-mail có đuôi @gmail.com"
    			},
        		"user-phone":{
    				required: "Hãy nhập số điện thoại",
    				rangelength: "Số điện thoại bao gồm 10 số"
    			},
        		"user-userName":{
    				required: "Hãy nhập username",
    				minlength: "username ít nhất 2 kí tự"
    			},
        		"user-password":{
    				required: "Hãy nhập password",
    				minlength: "Password ít nhất 5 kí tự"
    			},
    			"user-created":{
    				required: "Hãy nhập ngày tạo",
    			}
    		}
    	});
    })
		var date = new Date();
		
		var day = date.getDate();
		var month = date.getMonth() + 1;
		var year = date.getFullYear();
		
		if (month < 10) month = "0" + month;
		if (day < 10) day = "0" + day;
		
		var today = year + "-" + month + "-" + day;
		
		
		document.getElementById('the-date').value = today;
</script>

    <jsp:include page = "./footer/footer.jsp" flush = "true" />