<%@page contentType="text/html" pageEncoding="UTF-8"%>
  <!-- Start header section -->
  <jsp:include page = "./header/header.jsp" flush = "true" />
    <div class="content-wrapper">
      <div class="container-fluid">

        <div class="row mt-3">
          <div class="col-lg-12">
            <div class="card">
              <div class="card-body">
                <div class="card-title">Thêm tin tức</div>
                <hr>
                <form id="addForm" action="${pageContext.request.contextPath}/admin/new/add" method="post" enctype='multipart/form-data'>
                 
                  <div class="form-group">
                    <label for="new-title">Tên tin tức</label>
                    <input type="text" class="form-control" id="new-title" placeholder="Tên tin tức" name="new-title">
                  </div>
                  <div class="form-group">
                    <label for="new-content">Nội dung</label>
                    <textarea class="form-control" rows="4" id="new-content" name="new-content"></textarea>
                  </div>
                  <div class="form-group">
                    <label for="input-3">Hình ảnh</label>
                    <input type="file" class="form-control" id="input-18" placeholder="Địa chỉ hình ảnh" name="file">
                  </div>
	               <div class="form-group">
	                  <label for="input-4">Người đăng</label>
                  <div>
                    <select class="form-control valid" id="new-author" name="new-author" aria-invalid="false">
                        <option>Lê Thạch</option>
                        <option>Lan Ngọc</option>
                        <option>Kim Vy</option>
                    </select>
                  </div>
                  </div>
                  <div class="form-group">
	                  <label for="new-created">Ngày đăng</label>
	                  <input type="date" class="form-control" id="new-created" name="new-created">
	              </div>
                 <div class="form-footer">
                 	
                    <a class="btn btn-danger" href="${pageContext.request.contextPath}/admin/new/list">Hủy</a>
                    <button type="submit" class="btn btn-success"> Thêm</button>
                </div>
                </form>
              </div>
            </div>
          </div>
        </div>
        <div class="overlay toggle-menu"></div>
      </div>
    </div>
  <script>
  $().ready(function(){
	  $("#addForm").validate({
		  rules:{
			  "new-title":{
				  required: true,
			  },
			  "new-content":{
				  required: true,
			  },
			  "new-created":{
				  required: true,
			  },
			  "new-author":{
				  required: true,
			  },
		  },
		  messages:{
			  "new-title":{
				  required: "Hãy nhập tiêu đề",
			  },
			  "new-content":{
				  required: "Hãy nhập nội dung",
			  },
			  "new-created":{
				  required: "Hãy chọn ngày đăng",
			  },
			  "new-author":{
				  required: "Hãy chọn người đăng",
			  },
		  },
	  });
  })
		var date = new Date();
		
		var day = date.getDate();
		var month = date.getMonth() + 1;
		var year = date.getFullYear();
		
		if (month < 10) month = "0" + month;
		if (day < 10) day = "0" + day;
		
		var today = year + "-" + month + "-" + day;
		
		
		document.getElementById('the-date').value = today;
</script>
    <jsp:include page = "./footer/footer.jsp" flush = "true" />