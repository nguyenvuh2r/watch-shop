<%@page language="java" contentType="text/html; charset=utf-8" pageEncoding="UTF-8"%> 
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
  <!-- Start header section --> 
  <jsp:include page = "./header/header.jsp" flush = "true" /> 
    <div class="content-wrapper"> 
      <div class="container-fluid"> 
 
        <div class="row mt-3"> 
          <div class="col-lg-8"> 
            <div class="card"> 
              <div class="card-body"> 
                <div class="card-title">Thêm Admin</div> 
                <hr> 
                <form id="addForm" action="${pageContext.request.contextPath}/admin/admin/add" method="post"> 
                           
                  <div class="form-group"> 
                    <label for="admin-username">Username <span>(Nhập ít nhất 2 kí tự)</span></label> 
                    <input type="text" class="form-control" id="admin-username" placeholder="Username"  name="admin-username" > 
                  </div> 
                  <div class="form-group"> 
                    <label for="admin-password">Password</label> 
                    <input type="password" class="form-control" id="admin-password" placeholder="Password" name="admin-password" >
                    <br/>
                    <input type="checkbox" onclick="myFunction1()">Hiển thị mật khẩu
                    <script>function myFunction1() {
                    	  var x = document.getElementById("admin-password");
                    	  if (x.type === "password") {
                    	    x.type = "text";
                    	  } else {
                    	    x.type = "password";
                    	  }
                    	}</script> 
                  </div>                            
                 <div class="form-group"> 
                    <label for="admin-name">Tên Admin</label> 
                    <input type="text" class="form-control" id="admin-name" placeholder="Tên Admin" name="admin-name" > 
                  </div>	
                  <div class="form-footer"> 
					  <i class="fa fa-times"></i><a class="btn btn-danger" href="${pageContext.request.contextPath}/admin/admin/list">Hủy</a>
                    <button type="submit" class="btn btn-success"><i class="fa fa-check-square-o"></i> Thêm</button>
                </div>  
                </form> 
              </div> 
            </div> 
          </div> 
        </div> 
        <div class="overlay toggle-menu"></div> 
      </div> 
    </div> 
  
    <script  type="text/javascript">
    $().ready(function(){ 
    	$( "#addForm" ).validate({
      	  	rules: {
      		"admin-username": {
      	      required: true,
      	      minlength: 2,
      	    },
  		    "admin-password": {
  			  required: true,
  			  minlength: 5,
  			},
  		    "admin-name": "required"
      	  },
      	  messages:{
      		 "admin-username": {
      			required: "Hãy nhập username!!",
          	    minlength: "Username ít nhất 2 kí tự",
          	 },
      		 "admin-password": {
      			required: "Hãy nhập password",
      			minlength: "Password ít nhất 2 kí tự",
      	     },
      		 "admin-name": {
      			required: "Hãy nhập tên admin",
      		 },
      	  },
      	  
      	});
    	
    })
    
    </script>
   
    <jsp:include page = "./footer/footer.jsp" flush = "true" />