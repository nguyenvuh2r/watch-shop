package watch.webmvc.controller.admin;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import watch.webmvc.model.Catalog;
import watch.webmvc.model.Product;
import watch.webmvc.service.CategoryService;
import watch.webmvc.service.ProductService;
import watch.webmvc.service.UpFileService;
import watch.webmvc.service.impl.CategoryServicesImpl;
import watch.webmvc.service.impl.ProductServiceImpl;
import watch.webmvc.service.impl.UpFileServiceImpl;

@MultipartConfig(fileSizeThreshold = 1024 * 1024 * 2, // 2MB
		maxFileSize = 1024 * 1024 * 50, // 50MB
		maxRequestSize = 1024 * 1024 * 50) // 50MB

public class ProductAddController extends HttpServlet {
	private static final long serialVersionUID = 1L;
	ProductService productService = new ProductServiceImpl();
	UpFileService up = new UpFileServiceImpl();

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		CategoryService cateService = new CategoryServicesImpl();
		List<Catalog> cateList = cateService.getAll();
		req.setAttribute("catelist", cateList);
		RequestDispatcher dispatcher = req.getRequestDispatcher("/view/admin/addproduct.jsp");
		dispatcher.forward(req, resp);
	}


	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
			req.setCharacterEncoding("utf-8");
			resp.setContentType("text/html;charset=UTF-8");
			String product_cate = req.getParameter("product-cate");
			String product_name = req.getParameter("product-name");
			String product_price = req.getParameter("product-price");
			String product_status = req.getParameter("product-status");
			String product_desc = req.getParameter("product-desc");
			String product_content = req.getParameter("product-content");
			String product_discount = req.getParameter("product-discount");
			String product_day = req.getParameter("product-day");
			System.out.println(product_price);
			Product product = new Product();
			product.setCatalog_id(product_cate);
			product.setName(product_name);
			product.setPrice(product_price.replace(",",""));
			product.setStatus(product_status);
			product.setDescription(product_desc);
			product.setContent(product_content);
			product.setDiscount(product_discount);
			product.setCreated(product_day);
			
			String fileList = "";
			List<Part> lstFile = req.getParts().stream().
					filter(part->"file".equals(part.getName())).collect(Collectors.
					        toList());
			
			for(Part filePart: req.getParts()) {
				if(filePart.getSubmittedFileName() != null && filePart.getSubmittedFileName().length() > 0) {
					fileList += filePart.getSubmittedFileName().toString() + ";";
				}
			}
			
			if(fileList != "") {
				product.setImage_link(fileList);
			}
			up.saveFile(req);
			productService.insert(product);
			resp.sendRedirect(req.getContextPath() + "/admin/product/list");

	}
}
