package watch.webmvc.controller.admin;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import watch.webmvc.model.Boardnew;
import watch.webmvc.service.BoardnewService;
import watch.webmvc.service.UpFileService;
import watch.webmvc.service.impl.BoardnewServicesImpl;
import watch.webmvc.service.impl.UpFileServiceImpl;
@MultipartConfig(fileSizeThreshold = 1024 * 1024 * 2, // 2MB
maxFileSize = 1024 * 1024 * 50, // 50MB
maxRequestSize = 1024 * 1024 * 50) // 50MB
public class BoardnewAddController extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	BoardnewService boardnewService = new BoardnewServicesImpl();
	UpFileService up = new UpFileServiceImpl();
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		RequestDispatcher dispatcher = req.getRequestDispatcher("/view/admin/addboardnew.jsp");
		dispatcher.forward(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		req.setCharacterEncoding("utf-8");
		resp.setContentType("text/html;charset=UTF-8");
		String new_title = req.getParameter("new-title");
		String new_content = req.getParameter("new-content");
		String new_author = req.getParameter("new-author");
		String new_created = req.getParameter("new-created");
		Boardnew boardnew = new Boardnew();
		boardnew.setTitle(new_title);
		boardnew.setContent(new_content);
		boardnew.setAuthor(new_author);
		boardnew.setCreated(new_created);
		String fileList = "";
		List<Part> lstFile = req.getParts().stream().
				filter(part->"file".equals(part.getName())).collect(Collectors.
				        toList());
		System.out.println(new_title);
		for(Part filePart: req.getParts()) {
			if(filePart.getSubmittedFileName() != null && filePart.getSubmittedFileName().length() > 0) {
				fileList += filePart.getSubmittedFileName().toString() + ";";
			}
		}
		
		if(fileList != "") {
			boardnew.setImage_link(fileList);
		}
		up.saveFile(req);
		boardnewService.insert(boardnew);
		resp.sendRedirect(req.getContextPath() + "/admin/new/list");
	}

}
