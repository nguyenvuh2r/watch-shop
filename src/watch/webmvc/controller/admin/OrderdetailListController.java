package watch.webmvc.controller.admin;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import watch.webmvc.dto.OrderDetailDTO;
import watch.webmvc.model.Ordered;
import watch.webmvc.model.Product;
import watch.webmvc.service.OrderedService;
import watch.webmvc.service.ProductService;
import watch.webmvc.service.impl.OrderedServiceImpl;
import watch.webmvc.service.impl.ProductServiceImpl;

public class OrderdetailListController extends HttpServlet{
	private static final long serialVersionUID = 1L;
	OrderedService orderedServcie = new OrderedServiceImpl();
	ProductService productService = new ProductServiceImpl();
	@Override 
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException { 
		String id = req.getParameter("id");
		System.out.println(id);
		List<Ordered> orderedList = orderedServcie.getByIdTransaction(Integer.parseInt(id));
		
		List<Product> products = new ArrayList<Product>();
		List<OrderDetailDTO> orderDetail = new ArrayList<OrderDetailDTO>();
		System.out.println(products);
		
		for(Ordered ordered: orderedList)
		{
			Product product = new Product();
			product = productService.get(Integer.parseInt(ordered.getProduct_id()));
			OrderDetailDTO orderdt = new OrderDetailDTO();
			orderdt.setProductName(product.getName());
			orderdt.setQuantity(ordered.getQty());
			orderdt.setPrice(product.getPrice());
			orderDetail.add(orderdt);
		};
		
		req.setAttribute("orderDetail", orderDetail);
		System.out.println(orderDetail.toString());

		RequestDispatcher dispatcher = req.getRequestDispatcher("/view/admin/show-orderdetail.jsp"); 
		dispatcher.forward(req, resp); 
	}
}
