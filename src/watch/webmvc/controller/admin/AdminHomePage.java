package watch.webmvc.controller.admin;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import watch.webmvc.jdbc.connectDB;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class AdminHomePage extends HttpServlet {
	private static final long serialVersionUID = 1L;

    public AdminHomePage() {
        super();
    }	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		RequestDispatcher dispatcher = request.getRequestDispatcher("/view/admin/index.jsp");
		
		String char_labels = "";
		String char_background = "";
		String char_data = "";
		int bill_in_week = 0;
		float grow = 0f;
		
		int item_in_week = 0;
		float item_grow = 0f;
		List<String> lstProduct = new ArrayList<String>();
		List<String> lstProductQty = new ArrayList<String>();
		
		String sql = "SELECT p.name, o.qty, o.qty*CAST(p.price as DECIMAL(9,2)) AS total FROM product p, ordered o WHERE p.id = o.product_id";
		Connection con = connectDB.getConnect();
		try {
			PreparedStatement ps = con.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			while(rs.next())
			{
				char_labels += "\"" + rs.getString("name") + "\",";
				char_background += "getRandomColor(),";
				char_data += rs.getString("qty") + ",";
				
				lstProduct.add(rs.getString("name"));
				lstProductQty.add(rs.getString("total")); 
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		String sql_get_week_total_bills = "Select count(*) From transactions Where created BETWEEN DATEADD(DAY, -7, GETDATE()) AND DATEADD(DAY, 1, GETDATE())";
		String sql_get_total_bill = "Select count(*) From transactions";
		try {
			PreparedStatement ps = con.prepareStatement(sql_get_week_total_bills);
			ResultSet rs = ps.executeQuery();
			if(rs.next()) {
				bill_in_week = rs.getInt(1);
				
				PreparedStatement ps2 = con.prepareStatement(sql_get_total_bill);
				ResultSet rs2 = ps.executeQuery();
				if(rs2.next()) {
					int total_bill = rs2.getInt(1);
					if(total_bill > 0 ) {
						grow = (bill_in_week / total_bill) * 100;
					}
					else grow =0;
					
					
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		String sql_get_week_total_item = "Select count(*) From ordered, transactions Where ordered.transaction_id = transactions.id And transactions.created BETWEEN DATEADD(DAY, -7, GETDATE()) AND DATEADD(DAY, 1, GETDATE())";
		String sql_get_total_item = "Select count(*) From ordered, transactions Where ordered.transaction_id = transactions.id";
		try {
			PreparedStatement ps = con.prepareStatement(sql_get_week_total_item);
			ResultSet rs = ps.executeQuery();
			if(rs.next()) {
				item_in_week = rs.getInt(1);
				
				PreparedStatement ps2 = con.prepareStatement(sql_get_total_item);
				ResultSet rs2 = ps.executeQuery();
				if(rs2.next()) {
					int total_item = rs2.getInt(1);
					if(total_item > 0) {
						item_grow = (item_in_week / total_item) * 100;
					}
					else item_grow = 0;
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		request.setAttribute("char_labels", char_labels);
		request.setAttribute("char_background", char_background);
		request.setAttribute("char_data", char_data);
		
		request.setAttribute("lstProduct", lstProduct);
		request.setAttribute("lstProductQty", lstProductQty);
		
		request.setAttribute("bill_in_week", bill_in_week);
		request.setAttribute("grow", grow);
		
		request.setAttribute("item_in_week", item_in_week);
		request.setAttribute("item_grow", item_grow);
		dispatcher.forward(request,response);
	}

}
