package watch.webmvc.controller.admin;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import watch.webmvc.model.Admin;
import watch.webmvc.service.AdminService;
import watch.webmvc.service.impl.AdminServicesImpl;

public class AdminDeleteController extends HttpServlet {
	private static final long serialVersionUID = 1L;
	AdminService adminService = new AdminServicesImpl();

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String admin_id = req.getParameter("admin-id");
		HttpSession session = req.getSession();
		String usernameCurrent = (String) session.getAttribute("admin-username");
		int admin_id_int = Integer.parseInt(admin_id);
		Admin admin = adminService.get(admin_id_int);
		if (admin.getUsername().equals(usernameCurrent)) {
			req.setAttribute("errorMessage", "Bạn đã xóa chính mình !!!");
			req.setAttribute("adminlist", adminService.getAll());
			RequestDispatcher dispatcherUser = req.getRequestDispatcher("/view/admin/admin.jsp");
			dispatcherUser.forward(req, resp);
		} else {
			adminService.delete(admin_id);
			req.setAttribute("adminlist", adminService.getAll());
			RequestDispatcher dispatcherUser = req.getRequestDispatcher("/view/admin/admin.jsp");
			dispatcherUser.forward(req, resp);
		}
	}
}
