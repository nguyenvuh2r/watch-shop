package watch.webmvc.controller.admin;

import java.io.IOException;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import watch.webmvc.model.Catalog;
import watch.webmvc.model.Product;
import watch.webmvc.service.CategoryService;
import watch.webmvc.service.ProductService;
import watch.webmvc.service.UpFileService;
import watch.webmvc.service.impl.CategoryServicesImpl;
import watch.webmvc.service.impl.ProductServiceImpl;
import watch.webmvc.service.impl.UpFileServiceImpl;

/**
 * Servlet implementation class ProductEditController
 */
@MultipartConfig(fileSizeThreshold = 1024 * 1024 * 2, // 2MB
		maxFileSize = 1024 * 1024 * 50, // 50MB
		maxRequestSize = 1024 * 1024 * 50) // 50MB

public class ProductEditController extends HttpServlet {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	ProductService productService = new ProductServiceImpl();
	UpFileService up = new UpFileServiceImpl();

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		CategoryService cateService = new CategoryServicesImpl();
		List<Catalog> cateList = cateService.getAll();
		req.setAttribute("catelist", cateList);

		String id = req.getParameter("id");
		Product product = productService.get(Integer.parseInt(id));
		req.setAttribute("product", product);

		RequestDispatcher dispatcher = req.getRequestDispatcher("/view/admin/editproduct.jsp");
		dispatcher.forward(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		req.setCharacterEncoding("utf-8");
		resp.setContentType("text/html;charset=UTF-8");
		Product product = new Product();
		product.setId(req.getParameter("product-sku"));
		product.setCatalog_id(req.getParameter("product-cate"));
		product.setName(req.getParameter("product-name"));
		product.setPrice(req.getParameter("product-price"));
		product.setStatus(req.getParameter("product-status"));
		product.setDescription(req.getParameter("product-desc"));
		product.setContent(req.getParameter("product-content"));
		product.setDiscount(req.getParameter("product-discount"));
		product.setCreated(req.getParameter("product-day"));
		String fileList = "";
		List<Part> lstFile = req.getParts().stream().
				filter(part->"file".equals(part.getName())).collect(Collectors.
				        toList());
		
		for(Part filePart: req.getParts()) {
			if(filePart.getSubmittedFileName() != null && filePart.getSubmittedFileName().length() > 0) {
				fileList += filePart.getSubmittedFileName().toString() + ";";
			}
		}
		
		if(fileList != "") {
			product.setImage_link(fileList);
		}
		else
		{
			String image = productService.get(Integer.parseInt(req.getParameter("product-sku"))).getImage_link();
			product.setImage_link(image);
		}	
			
		
		
		
	
		up.saveFile(req);
		
		productService.edit(product);
		resp.sendRedirect(req.getContextPath() + "/admin/product/list");

	}
}