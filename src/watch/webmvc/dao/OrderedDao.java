package watch.webmvc.dao;

import java.util.List;

import watch.webmvc.model.Ordered;

public interface OrderedDao {
	void insert(Ordered ordered); 
	 
	void edit(Ordered ordered); 
	
	void delete(String id); 
 
	List<Ordered> getByIdTransaction(int id); 
	 
	Ordered get(String name); 
 
	List<Ordered> getAll();
}
