package watch.webmvc.service.impl;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.Part;

import watch.webmvc.service.UpFileService;

public class UpFileServiceImpl implements UpFileService {
	// private static final String  UPLOAD_DIR = "WebContent\\view\\admin\\assets\\images";
	public String extractFileName(Part part) {
		String contentDisp = part.getHeader("content-disposition");
		String[] items = contentDisp.split(";");
		for (String s : items) {
			if (s.trim().startsWith("filename")) {
				return s.substring(s.indexOf("=") + 2, s.length() - 1);
			}
		}
		return "";
	}

	public File getFolderUpload() {
		File folderUpload = new File(System.getProperty("user.home") + "/Uploads");
		
		if (!folderUpload.exists()) {
			folderUpload.mkdirs();
		}
		return folderUpload;
	}

	public void saveFile(HttpServletRequest request) {
		 String applicationPath = request.getServletContext().getRealPath("/images");
			
		 String basePath = applicationPath;
		 if(!Files.exists(Path.of(basePath))) {
			 try {
				Files.createDirectories(Path.of(basePath));
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		 }
		try {
			    for(Part filePart : request.getParts()) {
			    	String fileName = extractFileName(filePart);
					if(fileName != "") {
						filePart.write(basePath +"/"+ fileName);
					}
			    }
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ServletException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		request.setAttribute("message", "Upload File Success!");
	}

}
