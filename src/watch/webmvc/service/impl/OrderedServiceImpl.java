package watch.webmvc.service.impl;

import java.util.List;

import watch.webmvc.dao.OrderedDao;
import watch.webmvc.dao.impl.OrderedDaoImpl;
import watch.webmvc.model.Ordered;
import watch.webmvc.service.OrderedService;

public class OrderedServiceImpl implements OrderedService{
	OrderedDao orderDao = new OrderedDaoImpl();
	@Override
	public void insert(Ordered ordered) {
		orderDao.insert(ordered);
	}

	@Override
	public void edit(Ordered ordered) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void delete(String id) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public List<Ordered> getByIdTransaction(int id) {
		return orderDao.getByIdTransaction(id);
	}

	@Override
	public Ordered get(String name) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Ordered> getAll() {
		return orderDao.getAll();
	}

}
