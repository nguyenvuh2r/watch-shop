package watch.webmvc.service;

import java.io.File;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.Part;

public interface UpFileService {
	public String extractFileName(Part part);
	public File getFolderUpload();
	public void saveFile(HttpServletRequest request);
}	
