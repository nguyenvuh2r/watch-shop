package watch.webmvc.dto;

public class OrderDetailDTO {
	String productName;
	String price;
	int quantity;
	public OrderDetailDTO(String productName, String price, int quantity) {
		super();
		this.productName = productName;
		this.price = price;
		this.quantity = quantity;
	}
	public OrderDetailDTO() {
		
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public String getPrice() {
		return price;
	}
	public void setPrice(String price) {
		this.price = price;
	}
	public int getQuantity() {
		return quantity;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	@Override
	public String toString() {
		return "OrderDetailDTO [productName=" + productName + ", price=" + price + ", quantity=" + quantity + "]";
	}
	
}
